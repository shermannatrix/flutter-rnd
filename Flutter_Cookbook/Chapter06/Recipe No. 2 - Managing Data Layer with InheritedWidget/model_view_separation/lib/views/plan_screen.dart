import '../models/data_layer.dart';
import 'package:flutter/material.dart';

import '../plan_provider.dart';

class PlanScreen extends StatefulWidget {
	const PlanScreen({ Key? key }) : super(key: key);

	@override
	State<PlanScreen> createState() => _PlanScreenState();
}

class _PlanScreenState extends State<PlanScreen> {
	// final plan = Plan();
	late ScrollController scrollController;

	@override
	void initState() {
		super.initState();
		scrollController = ScrollController();
		scrollController.addListener(() {
			FocusScope.of(context).requestFocus(FocusNode());
		});
	}

	@override
	void dispose() {
		scrollController.dispose();
		super.dispose();
	}

	@override
	Widget build(BuildContext context) {
    final plan = PlanProvider.of(context);
		return Scaffold(
			appBar: AppBar(title: const Text('Master Plan')),
			body: Column(
			  children: <Widget> [
			    Expanded(
            child: _buildList()
          ),
          SafeArea(
            child: Text(plan.completenessMessage)
          ),
			  ],
			),
			floatingActionButton: _buildAddTaskButton(),
		);
	}

	Widget _buildList() {
    final plan = PlanProvider.of(context);
		return ListView.builder(
			controller: scrollController,
			itemCount: plan.tasks.length,
			itemBuilder: (context, index) => _buildTaskTile(plan.tasks[index]),
		);
	}

	Widget _buildAddTaskButton() {
    final plan = PlanProvider.of(context);
		return FloatingActionButton(
			child: const Icon(Icons.add),
			onPressed: () {
				setState(() {
					plan.tasks.add(Task());
				});
			},
		);
	}

	Widget _buildTaskTile(Task task) {
		return ListTile(
			leading: Checkbox(
				value: task.complete,
				onChanged: (selected) {
					setState(() {
						task.complete = selected;
					});
				}
			),
			title: TextFormField(
				initialValue: task.description,
        onFieldSubmitted: (text) {
          setState(() {
            task.description = text;
          });
        },
			),
		);
	}
}