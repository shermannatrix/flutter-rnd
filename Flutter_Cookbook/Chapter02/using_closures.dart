void callbackExample(void callback(String value)) {
	callback('Hello Callback');
}

typedef NumberGetter = int Function();

int powerOfTwo(NumberGetter getter) {
	return getter() * getter();
}

void consumeClosure() {
	final getFour = () => 4;
	final squared = powerOfTwo(getFour);
	print(squared);

	callbackExample((result) {
		print(result);
	});
}

void main() {
	consumeClosure();
}