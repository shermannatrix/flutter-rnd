void main() {
	String someString = "Happy string";
	print ("The string is: $someString");

	print("The string length is: ${someString.length}");
	
	// Omitting the curly brackets meant only the variable 
	//was evaluated, not the method on the variable.
	print("The string length is $someString.length");
}