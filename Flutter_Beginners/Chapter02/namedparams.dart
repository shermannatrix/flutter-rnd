String sayHappyBirthday(String name, {int age = 7}) {
	return "Happy Birthday $name! You are $age years old.";
}

void main() {
	var birthdayGreeting = sayHappyBirthday('Laura', age: 21);
	print(birthdayGreeting);
}