import 'dart:math';

class Position {
	late int x;
	late int y;

	double distanceFrom(Position dis) {
		var dx = dis.x - x;
		var dy = dis.y - y;
		return sqrt(dx * dx + dy * dy);
	}
}

class Perimeter {
	late int length;
	late int breadth;

	int get area => 2 * (length * breadth);
}

class PerimeterView extends Perimeter with Position {}

void main() {
	var origin = new Position()
		..x = 0
		..y = 0;
	
	var p = new PerimeterView()
		..x = 5
		..y = 5
		..length = 10
		..breadth = 11;
	
	print(p.distanceFrom(origin));
	print(p.area);
}